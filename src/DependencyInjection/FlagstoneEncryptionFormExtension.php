<?php
/**
 * FlagstoneEncryptionFormExtension.php
 *
 * @copyright 2022
 * @author Emmanuel Grosdemange <emmanuel.grosdemange57@gmail.com>
 */

namespace Flagstone\EncryptionFormBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class FlagstoneEncryptionFormExtension extends Extension
{
    /**
     * @param   array               $configs
     * @param   ContainerBuilder    $container
     * @throws  Exception
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__ . '/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}
