<?php

namespace Farvest\EncryptionFormBundle\EncryptionForm;

use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Interfaces\EncryptionDatabaseInterface;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\Exceptions\EncryptionClassUndefinedException;
use Flagstone\EncryptionDoctrineBridgeBundle\DoctrineEncryption\DatabaseEncryption;
use ReflectionException;
use Symfony\Component\Form\FormInterface;

/** *****************************************************************************************************************
 *  Class EncryptionForm
 *  -----------------------------------------------------------------------------------------------------------------
 *  Class to add fields with decipher data on forms.
 *  -----------------------------------------------------------------------------------------------------------------
 *  @package Farvest\EncryptionDoctrineBridgeBundle\EncryptionDoctrineBridge
 *  ***************************************************************************************************************** */
class EncryptionForm
{
    /**
     *  @var DatabaseEncryption
     *  ------------------------------------------------------------------------------------------------------------- */
    private $encryption;

    /** *************************************************************************************************************
     *  EncryptionForm constructor.
     *  -------------------------------------------------------------------------------------------------------------
     *  @param DatabaseEncryption $encryption
     *  ************************************************************************************************************* */
    public function __construct(Encryption $encryption)
    {
        $this->encryption = $encryption;
    }

    /** *************************************************************************************************************
     *  @param FormInterface &$form
     *  @param EncryptionDatabaseInterface $entity
     *  @param string $type
     *  @param string $field
     *  @param array $options
     *  @throws EncryptionClassUndefinedException
     *  @throws ReflectionException
     *  ************************************************************************************************************* */
    public function addDecryptField(FormInterface &$form, EncryptionDatabaseInterface $entity, string  $type, string $field, array $options)
    {
        $property = 'get'.ucfirst($field);

        $options = [];
        if (null !== $entity->$property()) {
            $data = $this->encryption->decryptData(get_class($entity), $field, $entity->$property());
            $options['data'] = $data;
        }

        $form->add(
            $field,
            $type,
            $options
        );
    }
}
